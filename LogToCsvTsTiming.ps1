﻿#Version 1.1
#Add Assembly for Save and Open dialog
Add-Type -AssemblyName System.Windows.Forms

#Create Opendialog if cancel chosen script stop
$Ui = New-Object -TypeName System.Windows.Forms.OpenFileDialog -Property @{ multiselect = $true;Filter = "Log files (*.log)|*.log|All files (*.*)|*.*" }

if($Ui.ShowDialog() -eq [System.Windows.Forms.DialogResult]::OK)  
{ 
    #Create the string concatenation From all logs file selected.
    $String = ''
    Foreach( $filename in $ui.FileNames)
	{
        $String += Get-Content -Path $filename
    }

    #regex Select all Step with 2 groups 1 for step name 1 for Time
    $regex = 'Successfully completed the action \((.{0,256})\).{0,128}time="([0-9:.-]*)"'
    $matches = [Regex]::Matches($string,$regex)

    $collection = New-Object System.Collections.ArrayList

    #For each Match we create the Csv column Step Time Duration
    Foreach( $match in $matches )
	{
        $collection.add( (New-Object psobject -Property ([ordered] @{ Step = $match.groups[1].value ; Time = $match.groups[2].value; Duration = '' })) ) | out-null
    }

    #we sort the Array by Time to calculate duration
    $collection = $collection | Sort-Object -Property Time 

    $i=1
    While( $i -lt $collection.count )
	{
        $collection[$i].duration = ((get-date $collection[$i].time) - (get-date $collection[$i-1].time)).TotalMinutes
        $i++
    }

    #We Save the arraylist in a windows csv (Delimiter = ;)
    $Ui = New-Object -TypeName System.Windows.Forms.SaveFileDialog  -Property @{Filter = "CSV files (*.csv)|*.csv"}
    if($Ui.ShowDialog() -eq [System.Windows.Forms.DialogResult]::OK)  
    { 
       $collection | Export-Csv $Ui.FileName -NoTypeInformation -Delimiter ";"
    }
}